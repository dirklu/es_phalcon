<?php
namespace app\controllers\count;
use app\controllers\ControllerBase;
use Elasticsearch\ClientBuilder;


class IndexController extends ControllerBase
{

    public function indexAction()
    {

        //读取配置的两种方式
        //$this->>config();
        //$this->getDI()->getConfig()->database;
        $this->view->disable();
        var_dump($this->config->toArray()['es_config']);
        $client = ClientBuilder::create()->setHosts($this->config->toArray()['es_config'])->build();
        $params['index'] = 'bx_live_' . date("Ym");
        $params['type']  = 'tal';
        $params['body']  = ['testField' => 'abc'];
        // Document will be indexed to my_index/my_type/my_id
        $response = $client->index($params);
        var_dump($response);

    }

    public function searchAction()
    {
        $client = ClientBuilder::create()->setHosts($this->config->toArray()['es_config'])->build();
        $params = [
            'index' => 'bx_live_' . date("Ym"),
            'type' => 'tal',
            'body' => [
                'query' => [
                    'match' => [
                        'testField' => 'abc'
                    ]
                ]
            ]
        ];
        $results = $client->search($params);
        var_dump($results);
    }

}

