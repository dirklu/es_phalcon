<?php
/**
 * Created by PhpStorm.
 * User: ZenGo
 * Date: 2017/8/6
 * Time: 4:06
 */
use Phalcon\Mvc\Dispatcher;

$di->set(
    'dispatcher',
    function () {
        $dispatcher = new Dispatcher();

        $dispatcher->setDefaultNamespace(
            'app\controllers'
        );

        return $dispatcher;
    }
);
