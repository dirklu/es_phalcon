<?php

$router = $di->getRouter();

// Define your routes here
$router->add(
    '/app/index/index',
    [
        'namespace'  => 'app\controllers\count',
        'controller' => 'Index',
        'action'     => 'index',
    ]
);
$router->add(
    '/app/index/search',
    [
        'namespace'  => 'app\controllers\count',
        'controller' => 'Index',
        'action'     => 'search',
    ]
);
$router->handle();
