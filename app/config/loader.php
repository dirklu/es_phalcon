<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        //$config->application->vendorDir
    ]
);
$loader->registerNamespaces(
    [
        'app\controllers' => APP_PATH . '/controllers/',
        'app\models'      => APP_PATH . '/models/',
    ]


);
$loader->registerFiles([$config->application->vendorDir.'autoload.php']);
$loader->register();
